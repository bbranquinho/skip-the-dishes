package br.com.skip.the.dishes.domain.order.commands

data class RequestOrderCommand(val orderId: String)
