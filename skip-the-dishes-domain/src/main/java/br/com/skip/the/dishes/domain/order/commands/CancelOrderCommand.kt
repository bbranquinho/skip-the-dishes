package br.com.skip.the.dishes.domain.order.commands

data class CancelOrderCommand(val orderId: String)
