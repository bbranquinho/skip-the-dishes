package br.com.skip.the.dishes.domain.order.commands

data class CreateOrderCommand(val customerId: String)
